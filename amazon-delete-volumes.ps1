﻿# clear host console
clear

# initialize connection
Set-AWSCredential -AccessKey AKIAIKX7VB55N3OHRD2Q -SecretKey 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 -StoreAs MyProfileName
Initialize-AWSDefaultConfiguration -ProfileName MyProfileName -Region eu-west-1

# cost for elemenets
$GP2price									= .11
$STANDARDprice								= .0550000009

# get all elements
$AllElements								= Get-EC2Volume
$Elements									= $AllElements
Write-Host "There are $($AllElements.Count) in AWS."




# exclude forbidden id elements
$ExcludeElements							= @("vol-0458ff6063c52bbbe", "vol-051db9fc83329ef3a", "vol-0b4090edfee7e6874", "vol-035b6f86e9d0261d0", "vol-089e36291360da0d6", "vol-8a7dd808", "vol-0d5df4ba9fbda1cca", "vol-01178497aa2f8aade")
Write-Host "Excluding $($ExcludeElements.Count) elements by id."
foreach($ExcludeElement in $ExcludeElements){
	$Elements								= $Elements | Where-Object {$_.VolumeId -ne $ExcludeElement}
}
Write-Host "Now we work with $($Elements.Count) elements."




# exclude forbidden state of elements
Write-Host "$(($Elements | Where-Object {$_.State -eq "in-use"}).Count) elements are in-use state."
$Elements									= $Elements | Where-Object {$_.State -ne "in-use"}
Write-Host "Now we work with $($Elements.Count)"


# seperate by elements types
$VolumesGP2									= $Elements | Where-Object {$_.VolumeType -eq 'gp2'}
$VolumesSTANDARD							= $Elements | Where-Object {$_.VolumeType -eq 'standard'}


Remove-Variable -Name SizeOfGP2
$VolumesGP2 | ForEach-Object {
	$SizeOfGP2								+= $_.Size
}

Remove-Variable -Name SizeOfSTANDARD
$VolumesSTANDARD | ForEach-Object {
	$SizeOfSTANDARD							+= $_.Size
}


Write-Host "Size of deleted Volumes in SSD: $SizeOfGP2 GB"
Write-Host "Size of deleted Volumes in Standard: $SizeOfSTANDARD GB"

Write-Host "Price of deleted Volumes in SSD: $($SizeOfGP2 * $GP2price) $"
Write-Host "Price of deleted Volumes in Standard: $($SizeOfSTANDARD * $STANDARDprice) $"



$Elements | Remove-EC2Volume -Force