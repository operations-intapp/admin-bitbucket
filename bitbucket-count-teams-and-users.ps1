﻿clear

function ConvertPSObjectToHashtable
{
    param (
        [Parameter(ValueFromPipeline)]
        $InputObject
    )

    process
    {
        if ($null -eq $InputObject) { return $null }

        if ($InputObject -is [System.Collections.IEnumerable] -and $InputObject -isnot [string])
        {
            $collection = @(
                foreach ($object in $InputObject) { ConvertPSObjectToHashtable $object }
            )

            Write-Output -NoEnumerate $collection
        }
        elseif ($InputObject -is [psobject])
        {
            $hash = @{}

            foreach ($property in $InputObject.PSObject.Properties)
            {
                $hash[$property.Name] = ConvertPSObjectToHashtable $property.Value
            }

            $hash
        }
        else
        {
            $InputObject
        }
    }
}

[System.Collections.ArrayList]$arrayofmembers=@()


# HTTP
# build authentication header
$user												= 'admin-bitbucket-aevi'
$pass												= 'U7jLCNjv6zEcEQfnjQkj'
$pair												= "$($user):$($pass)"

$encodedCreds										= [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$headers											= @{
	Authorization = "Basic $encodedCreds"
}
	
# endpoint to get all teams where admin-bitbucket-aevi is member
$uri												= 'https://bitbucket.org/api/1.0/user/privileges'
$response											= Invoke-WebRequest -Method GET -Headers $headers -Uri $uri

$convertedfromjson = $response.content | ConvertFrom-Json
$allteamshash = $convertedfromjson | ConvertPSObjectToHashtable




ForEach ($team in $allteamshash.teams.Keys) {

	$uri="https://bitbucket.org/api/1.0/groups/$team"
    
	# query API for team membership
	$response										= Invoke-WebRequest -Method GET -Headers $Headers -Uri $uri
	$getAllTeamMembersJson							= $response.Content | ConvertFrom-Json


	ForEach ($member in $getAllTeamMembersJson.members) {
        $arrayofmembers.Add($member.username) | Out-Null
    }
    
    

}


$numberofteams= ($allteamshash.teams.Keys | Measure-Object -Line).Lines
$numberofmembers=($arrayofmembers | Sort-Object -Unique | Measure-Object -Line).Lines

"In BitBucket Cloud are: " + $numberofteams + " teams"
"In BitBucket Cloud are: " + $numberofmembers + " usernames"
