﻿# clear host console
clear



# .NET methods for hiding/showing the console in the background
Add-Type -Name Window -Namespace Console -MemberDefinition '
[DllImport("Kernel32.dll")]
public static extern IntPtr GetConsoleWindow();

[DllImport("user32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'
function Hide-Console
{
    $consolePtr = [Console.Window]::GetConsoleWindow()
    #0 hide
    [Console.Window]::ShowWindow($consolePtr, 0)
}
Hide-Console



# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku



# converts pipeline to hashtable
function ConvertPSObjectToHashtable
{
    param (
        [Parameter(ValueFromPipeline)]
        $InputObject
    )

    process
    {
        if ($null -eq $InputObject) { return $null }

        if ($InputObject -is [System.Collections.IEnumerable] -and $InputObject -isnot [string])
        {
            $collection = @(
                foreach ($object in $InputObject) { ConvertPSObjectToHashtable $object }
            )

            Write-Output -NoEnumerate $collection
        }
        elseif ($InputObject -is [psobject])
        {
            $hash = @{}

            foreach ($property in $InputObject.PSObject.Properties)
            {
                $hash[$property.Name] = ConvertPSObjectToHashtable $property.Value
            }

            $hash
        }
        else
        {
            $InputObject
        }
    }
}





# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku




# HTTP
# build authentication header

#myself
$user												= 'marcel.mraz'
$pass												= 'Flnetpro100'

$pair												= "$($user):$($pass)"
$encodedCreds										= [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$headers											= @{
	Authorization = "Basic $encodedCreds"
}


# endpoint of rest API
$uri												= 'https://aevi-tools.atlassian.net/wiki/rest/api/content?spaceKey=~marcel.mraz'									# confluence


$postParams = '
{
	"type": "page",
	"title": "' + $title + '",
	"space": {
		"key": "~marcel.mraz"
	},
	"body": {
		"storage": {
			"value": "<p>This is a new page</p>",
			"representation": "storage"
		}
	}
}
'


$response										= Invoke-WebRequest -Method GET -Headers $headers -Uri $uri #-Body $postParams -ContentType 'application/json'
$response = $response.Content | ConvertFrom-Json | ConvertPSObjectToHashtable



ForEach ($pageid in $response.results.id) {
    $uri												= "https://aevi-tools.atlassian.net/wiki/rest/api/content/$pageid/"									# confluence
    $response										= Invoke-WebRequest -Method DELETE -Headers $headers -Uri $uri #-Body $postParams -ContentType 'application/json'
    $response.StatusCode
 }
