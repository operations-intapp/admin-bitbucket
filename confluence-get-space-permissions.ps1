﻿function ConvertPSObjectToHashtable
{
	param (
		[Parameter(ValueFromPipeline)]
		$InputObject
	)

	process
	{
		if ($null -eq $InputObject) { return $null }

		if ($InputObject -is [System.Collections.IEnumerable] -and $InputObject -isnot [string])
		{
			$collection = @(
				foreach ($object in $InputObject) { ConvertPSObjectToHashtable $object }
			)

			Write-Output -NoEnumerate $collection
		}
		elseif ($InputObject -is [psobject])
		{
			$hash = @{}

			foreach ($property in $InputObject.PSObject.Properties)
			{
				$hash[$property.Name] = ConvertPSObjectToHashtable $property.Value
			}

			$hash
		}
		else
		{
			$InputObject
		}
	}
}


# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku
clear


# HTTP
	# build authentication header

	# ask for credentials
	$user												= Read-Host -Prompt 'Enter your username to Atlassian'
	$pass												= Read-Host -Prompt 'Enter your password to Atlassian' -AsSecureString
		# decode pass
		$pass												= [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pass)
		$pass												= [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($pass)

	$pair												= "$($user):$($pass)"
	$encodedCreds										= [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
	$headers											= @{
		Authorization = "Basic $encodedCreds"
	}



# returns users email
function getEmail {
	param(
		[string]$UserInput
	)

	$uri										= 'https://aevi-tools.atlassian.net/wiki/rest/api/user?username=' + $UserInput + '&expand=details.personal'
	$response									= Invoke-WebRequest -Method GET -Headers $headers -Uri $uri #-Body $postParams -ContentType 'application/json'
	$responseJSON								= $response.Content
	$responseHASH								= $response.Content | ConvertFrom-Json | ConvertPSObjectToHashtable
    
	return $responseHASH.details.personal.email
}


# returns group members
function getGroupMembers {
	param(
		[string]$UserInput
	)

	$uri										= 'https://aevi-tools.atlassian.net/wiki/rest/api/group/' + $UserInput + '/member'
	$response									= Invoke-WebRequest -Method GET -Headers $headers -Uri $uri #-Body $postParams -ContentType 'application/json'
	$responseJSON								= $response.Content
	$responseHASH								= $response.Content | ConvertFrom-Json | ConvertPSObjectToHashtable
    
	return $responseHASH.results.username
}

# type of permission: delete, create, read, administer, restrict_content, export
Write-Host "There are following type of operations:`n"
Write-Host -ForegroundColor "yellow" "`t[D]elete`n`t[C]reate`n`t[R]ead`n`t[A]dminister`n`t[RESTRICT]_content`n`t[E]xport"
Write-Host -ForegroundColor "white" ""

while ($true) {
	$answer										= Read-Host -Prompt 'Enter operation which are you looking for'
	if ($answer -Cmatch "^([DCRAE]{1}|RESTRICT)$") {
		
		switch ($answer) {
			"D" {$operation='delete'}
			"C" {$operation='create'}
			"R" {$operation='read'}
			"A" {$operation='administer'}
			"E" {$operation='export'}
			"RESTRICT" {$operation='restrict_content'}
		}
		break
		
	} else {
		continue
	}
}




# get all space keys
$start												= 0
$limit												= 500
remove-variable spacekeys
while ($true) {

		# build an uri
		$uri										= 'https://aevi-tools.atlassian.net/wiki/rest/api/space?start=' + $start + '&limit=' + $limit									# confluence

		# get a response
		$response									= Invoke-WebRequest -Method GET -Headers $headers -Uri $uri #-Body $postParams -ContentType 'application/json'
		$responseJSON								= $response.Content
		$responseHASH								= $response.Content | ConvertFrom-Json | ConvertPSObjectToHashtable

		# if pagination limit was exceeded
		if (($responseHASH.results).Length -ge $limit) {
				$spacekeys							+= ($responseHASH.results).key
				$start								= $start + $limit
				continue
		} else {
				$spacekeys							+= ($responseHASH.results).key	
				break
		}
}




Write-Host `n`n"There are: "$($spacekeys.Length)" space(s)."`n`n





# get all space permissions
$start												= 0
$limit												= 500
ForEach ($spacekey in $spacekeys) {

	$uri											= 'https://aevi-tools.atlassian.net/wiki/rest/api/space?spaceKey=' + $spacekey + '&expand=permissions'

	# get a response
	$response									= Invoke-WebRequest -Method GET -Headers $headers -Uri $uri #-Body $postParams -ContentType 'application/json'
	$responseJSON								= $response.Content
	$responseHASH								= $response.Content | ConvertFrom-Json | ConvertPSObjectToHashtable
	
	

	# iterate each space
	$spaces										= $responseHASH
	ForEach ($space in $spaces) {
		
		$indent									= $false
		
		# basic info about space		
		Write-Host -NoNewLine `n`nName of space:`t$($space.results.name)`nKey of space:`t$($space.results.key)`nType of space:`t$($space.results.type)`nLink to space:`t$($space.results._links.self)`nAdministrators:`t
		
		# iterate each space permissions
		ForEach ($permission in $space.results.permissions) {

			# mine permissions
			if ($permission.operation.operation -eq $operation) {
				
				# in case of user query his email
				if ($permission.subjects.user.results.username) {

					if ($indent) {
						Write-Host `t`t`t`t$(getEmail ($permission.subjects.user.results.username))

					} else {
						Write-Host $(getEmail ($permission.subjects.user.results.username))
						$indent									= $true
					}
				
				# in case of group query members and their emails
				} elseif ($permission.subjects.group.results.name) {
					$GroupMembers						= getGroupMembers $permission.subjects.group.results.name
					ForEach ($GroupMember in $GroupMembers) {
					
						if ($indent) {
							Write-Host `t`t`t`t'('($permission.subjects.group.results.name)')'$(getEmail $GroupMember)

						} else {
							Write-Host '('($permission.subjects.group.results.name)')'$(getEmail $GroupMember)
							$indent									= $true
						}
					}
				}
			}
		}
	}
}
