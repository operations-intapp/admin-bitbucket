﻿# .NET methods for hiding/showing the console in the background
Add-Type -Name Window -Namespace Console -MemberDefinition '
[DllImport("Kernel32.dll")]
public static extern IntPtr GetConsoleWindow();

[DllImport("user32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'
function Hide-Console
{
    $consolePtr = [Console.Window]::GetConsoleWindow()
    #0 hide
    [Console.Window]::ShowWindow($consolePtr, 0)
}
Hide-Console



# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku


#######################################################################################################################################################################################################################################


# HTTP
# build authentication header
$user												= 'marcel.mraz'
$pass												= 'Flnetpro100'
$pair												= "$($user):$($pass)"

$encodedCreds										= [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$headers											= @{
	Authorization = "Basic $encodedCreds"
}




# entered uri
$uri='https://aevi-tools.atlassian.net/rest/api/2/issue/INTAPP-1884'

# # get relates to empl
$request=Invoke-WebRequest -Headers $Headers -Method GET -Uri $uri
$json = $request.content | ConvertFrom-Json
$employeeKey = $json.Fields.Issuelinks.Inwardissue.Key




# mine employee information like a name, surname, email

$request=Invoke-WebRequest -Headers $Headers -Method GET -Uri "https://aevi-tools.atlassian.net/rest/api/2/issue/$employeeKey"
$json = $request.content | ConvertFrom-Json
$name=$json.Fields.Customfield_12300

# check value if is null or empty
if( ([string]::IsNullOrEmpty($name) ) ) {
'is null or empty'
}
$surname=$json.Fields.Customfield_12301
$email=$json.Fields.Customfield_12302


$name
$surname
$email


$fullname="$name $surname"

# check consistence with active directory
(Get-ADUser -Filter {Name -like $fullname} -Properties Mail).Mail


# check, if this user already exists
$request=Invoke-WebRequest -Headers $Headers -Method GET -Uri "https://aevi-tools.atlassian.net/rest/api/2/user/search?username=$email"


if($request.RawContentLength -le 2) {
    'is null or empty'
}




# create user
#########################################################################################################################################################
#########################################################################################################################################################
#########################################################################################################################################################

$params = @{
name="TestUser"
password="654321"
emailAddress="fji73345@loaoa.com"
displayName="MACHO"
#applicationKeys=@("jira-software")
#notification="true"
}
$params = $params | ConvertTo-Json


$response=Invoke-WebRequest -Headers $Headers -Method POST -ContentType "application/json" -Uri "https://aevi-tools.atlassian.net/rest/api/2/user" -Body $params
$response.StatusCode
$response.Content | ConvertFrom-Json


# add user to group
$params = @{
name="TestUser"
}
$params = $params | ConvertTo-Json

$response=Invoke-WebRequest -Headers $Headers -Method POST -ContentType "application/json" -Uri "https://aevi-tools.atlassian.net/rest/api/2/group/user?groupname=testing-group" -Body $params
$response.StatusCode
$response.Content | ConvertFrom-Json



# send email
$smtpServer = "ms.wincor-nixdorf.cz"
$smtpFrom = "operations-intapp <operations-intapp@aevi.com>"
$smtpTo = "marcel.mraz@aevi.cz"
$messageSubject = "QuickStart guide"

$message = New-Object System.Net.Mail.MailMessage $smtpfrom, $smtpto
$message.Subject = $messageSubject
$message.IsBodyHTML = $true
$message.Body = '
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
	<head>
		<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
		<meta charset="UTF-8">
		<meta name=Generator content="Microsoft Word 14 (filtered medium)">
	</head>
<body>
lala<b>dd</b>
</body>
</html>
'

$smtp = New-Object Net.Mail.SmtpClient($smtpServer)
$smtp.Send($message)
