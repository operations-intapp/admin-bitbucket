﻿# clear host console
clear



# .NET methods for hiding/showing the console in the background
Add-Type -Name Window -Namespace Console -MemberDefinition '
[DllImport("Kernel32.dll")]
public static extern IntPtr GetConsoleWindow();

[DllImport("user32.dll")]
public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);
'
function Hide-Console
{
    $consolePtr = [Console.Window]::GetConsoleWindow()
    #0 hide
    [Console.Window]::ShowWindow($consolePtr, 0)
}
Hide-Console



# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku



# converts pipeline to hashtable
function ConvertPSObjectToHashtable
{
    param (
        [Parameter(ValueFromPipeline)]
        $InputObject
    )

    process
    {
        if ($null -eq $InputObject) { return $null }

        if ($InputObject -is [System.Collections.IEnumerable] -and $InputObject -isnot [string])
        {
            $collection = @(
                foreach ($object in $InputObject) { ConvertPSObjectToHashtable $object }
            )

            Write-Output -NoEnumerate $collection
        }
        elseif ($InputObject -is [psobject])
        {
            $hash = @{}

            foreach ($property in $InputObject.PSObject.Properties)
            {
                $hash[$property.Name] = ConvertPSObjectToHashtable $property.Value
            }

            $hash
        }
        else
        {
            $InputObject
        }
    }
}





# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku



# initialize spaces
function InitializeSpace {
param([string]$UserInput)

	# lock textboxes
	$TextAreaInput.Enabled								= $false
	$TextAreaOutput.Enabled								= $false
	$TextAreaOutput.Clear()
	$Form.Controls.Add($TextAreaOutput)


	# user input
	$spaceKeys											= $UserInput -split "`n"
	if ($RadioButton1.Checked -eq $true) {$titles=@('PM', 'Requirements', 'DEV', 'TEST', 'Delivery', 'Operation', 'Service')}
	elseif ($RadioButton2.Checked -eq $true) {$titles=@('Marketing', 'Requirements', 'DEV', 'TEST', 'Delivery', 'Operation', 'Service')}

	# show progress bar
	$Form.Controls.Add($progressbar)

	# reset progress bar
	$progressbar.Minimum								= 0	
	$progressbarMax										= ($titles | Measure-Object -Line | Select-Object -Property Lines).Lines
	$progressbar.Maximum								= $progressbarMax
	$progressbar.Value									= 0


	# HTTP
	# build authentication header

	#myself
	$user												= 'marcel.mraz'
	$pass												= 'Flnetpro100'

	$pair												= "$($user):$($pass)"
	$encodedCreds										= [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
	$headers											= @{
		Authorization = "Basic $encodedCreds"
	}


	# endpoint of rest API
	$uri												= 'https://aevi-tools.atlassian.net/wiki/rest/api/content/'									# confluence




	ForEach ($spaceKey in $spaceKeys) {
	
		ForEach ($title in $titles) {

			$postParams = '
			{
				"type": "page",
				"title": "' + $title + '",
				"space": {
					"key": "' + $spacekey + '"
				},
				"body": {
					"storage": {
						"value": "<p>This is a new page</p>",
						"representation": "storage"
					}
				}
			}
			'
			$progressbar.Value++
			
			$statusCode = try { $response = Invoke-WebRequest -Method POST -Headers $headers -Uri $uri -Body $postParams -ContentType 'application/json' } catch {
				$_.Exception.Response.StatusCode.Value__}

			if (!$statusCode) {
				$result											= "$spaceKey/$title was successfully created (" + $response.StatusCode + ")"
				$TextAreaOutput.SelectionColor = 'Green'
				$TextAreaOutput.AppendText("${result}`n")
			} else {
				$result											= "$spaceKey/$title responded via unhandled http status code (" + $statusCode + ")"
				$TextAreaOutput.SelectionColor = 'Red'
				$TextAreaOutput.AppendText("${result}`n")
			}
			
			Remove-Variable statusCode, response, result
		}
	}
	
	$TextAreaOutput.Enabled								= $true
	$TextAreaInput.Enabled								= $true
}





# label
$Label1													= New-Object Windows.Forms.Label
$Label1.Location										= '20, 20'
$Label1.Size											= '250, 20'
$Label1.TextAlign										= 'MiddleLeft' #'MiddleLeft'
$Label1.Font											= New-Object System.Drawing.Font("Microsoft Sans Serif ",8,[System.Drawing.FontStyle]::regular)	# Font styles are: Regular, Bold, Italic, Underline, Strikeout
$Label1.Text											= "Name of new space(s): "
$Label1.BorderStyle										= 'none' # FixedSingle, Fixed3D, None


# textarea
$TextAreaInput											= New-Object System.Windows.Forms.RichTextBox
$TextAreaInput.Location									= '20, 45'
$TextAreaInput.Size										= '350, 75'
$TextAreaInput.BorderStyle								= 'FixedSingle'
$TextAreaInput.Text										= ""
$TextAreaInput.Enabled									= $true

# groupbox menu
$groupBox1												= New-Object System.Windows.Forms.GroupBox
$groupBox1.Location										= '400, 45'
$groupBox1.Size											= '200, 75'
$groupBox1.Text											= "Choose type of product:"

# radio buttons
$RadioButton1											= New-Object System.Windows.Forms.RadioButton
$RadioButton1.Location									= '25, 25'
$RadioButton1.Size										= '150, 15'
$RadioButton1.Checked									= $true
$RadioButton1.Text										= "Project"

$RadioButton2											= New-Object System.Windows.Forms.RadioButton
$RadioButton2.Location									= '25, 45'
$RadioButton2.Size										= '150, 15'
$RadioButton2.Checked									= $false
$RadioButton2.Text										= "Product"
$groupBox1.Controls.Add($Radiobutton1)
$groupBox1.Controls.Add($Radiobutton2)


# button
$button1												= New-Object System.Windows.Forms.Button
$button1.Name											= 'initialize-space'
$button1.Location										= '20, 140'
$button1.Size											= '100, 25'
$button1.Text											= 'initialize-space'
$button1.TextAlign										= 'MiddleCenter'
#$button1.Image											= [System.Convert]::FromBase64String($Ico_UpdateButton)
#$button1.ImageAlign									= 'MiddleCenter'
$button1.UseVisualStyleBackColor						= $True
$button1.Add_MouseHover($ShowToolTip)       
$button1.Add_Click({if ($TextAreaInput.Text -ne '') {
						InitializeSpace $TextAreaInput.Text}})


# textarea
$TextAreaOutput											= New-Object System.Windows.Forms.RichTextBox
$TextAreaOutput.Location								= '20, 185'
$TextAreaOutput.Size									= '450, 100'
$TextAreaOutput.BorderStyle								= 'FixedSingle'
$TextAreaOutput.Text									= $result
$TextAreaOutput.Enabled									= $true


# progress bar
$progressbar											= New-Object System.Windows.Forms.ProgressBar
$progressbar.Location									= New-Object System.Drawing.Size(250, 300) #X = (formWidth/2) - (labelWidth/2)
$progressbar.Size										= New-Object System.Drawing.Size(200, 25)
$progressbar.Visible									= $true





#----------------------------------------------------------#
#   OBJEKT - FORMULAR                                      #
#----------------------------------------------------------#
$Form														= New-Object 'Windows.Forms.Form'
$Form.Text													= 'Create pages in new spaces'						# titulek formulare
$Form.Width									    			= 700												# sirka formulare
$Form.Height												= 400												# vyska formulare
#$Form.MinimumSize											= '500, 500'										# minimalni velikost
#$Form.MaximumSize											= '5000, 5000'										# maximalni velikost
$Form.MaximizeBox 											= $False											# povoluje / zakazuje maximalizaci
$Form.MinimizeBox											= $True												# povoluje / zakazuje minimalizacni tlacitko
#$Form.AutoScroll											= $True												# povoluje / zakazuje scroll
#$Form.AutoSize												= $True												# povoluje / zakazuje automatickou upravu velikosti
#$Form.AutoSizeMode											= 'GrowOnly'										# typ zmeny velikosti "GrowAndShrink", "GrowOnly" (autosize on)
#$Form.WindowState											= 'Maximized'										# vychozi velikost: Maximized, Minimized, Normal
$Form.SizeGripStyle											= 'Hide'											# Auto, Hide, Show
$Form.ShowInTaskbar											= $True												# povoluje / zakazuje zobrazeni ikony v taskbaru
$Form.Opacity												= 1.0												# pruhlednost formulare 1.0 is fully opaque; 0.0 is invisible
$Form.StartPosition											= 'CenterScreen'									# CenterScreen, WindowsDefaultLocation, WindowsDefaultBounds, CenterParent, Manual
#$Form.Location												= New-Object System.Drawing.Size(150, 50)			# pri hodnote Form.StartPosition = manual nastavuje vychozi lokaci
#$Form.BackColor											= '#FFFFFF'											# barva pozadi
#$Form.ForeColor											= '#FFFFFF'											# barva pozadi
#$Form.Font													= New-Object System.Drawing.Font("Calibri",10,[System.Drawing.FontStyle]::Regular)	# Font styles are: Regular, Bold, Italic, Underline, Strikeout
#$Form.Icon													= New-Object system.drawing.icon("$dir2img\bitbucket\bitbucket_2636363.ico")		# ikona


#----------------------------------------------------------#
#   ZOBRAZ TLACITCKA, MENU, LABELY                         #
#----------------------------------------------------------#
$Form.Controls.Add($Label1)
$Form.Controls.Add($TextAreaInput)
$Form.Controls.Add($groupBox1)
$Form.Controls.Add($button1)
#$Form.Controls.Add($TextAreaOutput)
#$Form.Controls.Add($progressbar)









#----------------------------------------------------------#
#   ZOBRAZ FORMULAR                                        #
#----------------------------------------------------------#
$Form.FormBorderStyle										= 'Fixed3D'											# zakazuje menit velikost objektu
$Form.[System.Windows.Forms.Application]::EnableVisualStyles()													# tvori hezci tlacitka
#$Form.Icon													= New-Object system.drawing.icon ('.\icon.ico')		# ikona
$Form.ShowDialog()																								# povinna direktiva pro zobrazeni formulare, powershell ceka, az formular neco splni

