﻿# Load Assemblies
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")

# Init Form
$Form = New-Object System.Windows.Forms.Form
$Form.width = 400
$Form.height = 600
$Form.Text = "Add Resource"

# Init ProgressBar
$pbrTest = New-Object System.Windows.Forms.ProgressBar
$pbrTest.Maximum = 100
$pbrTest.Minimum = 0
#$pbrTest.Value = 100
$pbrTest.Location = New-Object System.Drawing.Size(10,10)
$pbrTest.size = new-object System.Drawing.Size(100,50)
$i = 0
$Form.Controls.Add($pbrTest)

# Button
$btnConfirm = new-object System.Windows.Forms.Button
$btnConfirm.Location = new-object System.Drawing.Size(120,10)
$btnConfirm.Size = new-object System.Drawing.Size(100,30)
$btnConfirm.Text = "Start Progress"
$Form.Controls.Add($btnConfirm)


# Button Click Event to Run ProgressBar
$btnConfirm.Add_Click({
    
    While ($i -le 100) {
        $pbrTest.Value = $i
        Start-Sleep -m 1
        "VALLUE EQ"
        Write-Host $i
        $i += 1
    }
})


# Show Form
#$Form.Add_Shown({$Form.Activate()})
$Form.ShowDialog()

# DOESN'T WORK: Run ProgressBar after Form loaded

<#
$Form.Add_Shown({
    
    While ($i -le 5) {
        $pbrTest.Value = $i
        Start-Sleep -s 1
        "VALLUE EQ"
        $i
        $i += 1
    }
})


#>