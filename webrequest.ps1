﻿clear

function ConvertPSObjectToHashtable
{
    param (
        [Parameter(ValueFromPipeline)]
        $InputObject
    )

    process
    {
        if ($null -eq $InputObject) { return $null }

        if ($InputObject -is [System.Collections.IEnumerable] -and $InputObject -isnot [string])
        {
            $collection = @(
                foreach ($object in $InputObject) { ConvertPSObjectToHashtable $object }
            )

            Write-Output -NoEnumerate $collection
        }
        elseif ($InputObject -is [psobject])
        {
            $hash = @{}

            foreach ($property in $InputObject.PSObject.Properties)
            {
                $hash[$property.Name] = ConvertPSObjectToHashtable $property.Value
            }

            $hash
        }
        else
        {
            $InputObject
        }
    }
}


# import libraries
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")					# importuje knihovnu system.windows.forms
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")							# importuje knihovnu pro vykreslovani obrazku
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")							# import knihovny microsoft.visualbasic
Add-Type -AssemblyName "System.Windows.Forms"														# importuje knihovnu system.windows.forms
Add-Type -Assemblyname "System.Drawing"																# importuje knihovnu pro vykreslovani obrazku



# HTTP
# build authentication header

# bitbucket
#$user												= 'admin-bitbucket-aevi'
#$pass												= 'U7jLCNjv6zEcEQfnjQkj'	

# atlassian administrator
#$user												= 'techacc-jirareindexservice'
#$pass												= 'GCCCi0MJzr3SOsX8'

#myself
$user												= 'marcel.mraz'
$pass												= 'Flnetpro100'

$pair												= "$($user):$($pass)"
$encodedCreds										= [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$headers											= @{
	Authorization = "Basic $encodedCreds"
}

$postParams = '
{
	"type": "page",
	"title": "page VIA API",
	"space": {
		"key": "~marcel.mraz"
	},
	"body": {
		"storage": {
			"value": "<p>This is a new page</p>",
			"representation": "storage"
		}
	}
}
'
    

# endpoint of rest API
#$uri												= 'https://bitbucket.org/api/1.0/user/privileges'											# bitbucket
#$uri												= 'https://aevi-tools.atlassian.net/rest/api/2/issue/INTAPP-996'							# jira
#$uri												= 'https://aevi-tools.atlassian.net/rest/api/content/135090675/property'					# confluence
#$uri												= 'https://aevi-tools.atlassian.net/wiki/rest/api/content/135090675/child/attachment'		# confluence
#$uri												= 'https://aevi-tools.atlassian.net/wiki/rest/prototype/1/content/135090675/attachments'	# confluence
$uri												= 'https://aevi-tools.atlassian.net/wiki/rest/api/content/'									# confluence


$response											= Invoke-WebRequest -Method POST -Headers $headers -Uri $uri -Body $postParams -ContentType 'application/json'
$responseJSON										= $response.Content
$responseHASH										= $response.Content | ConvertFrom-Json | ConvertPSObjectToHashtable




$responseJSON
$responseHASH



